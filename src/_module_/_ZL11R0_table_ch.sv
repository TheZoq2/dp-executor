module _ZL11R0_table_ch31
		#( parameter output_size = 64
		)
		( clk
		, addr
		, result
		);
	input clk;
	localparam input_size = 3;
	input[input_size-1:0] addr;
	output reg[output_size-1:0] result;
	logic[output_size-1:0] data[6:0];
	initial begin
		data[0] = (output_size'(64'd41167261));
		data[1] = (output_size'(64'd45798877));
		data[2] = (output_size'(64'd41755102));
		data[3] = (output_size'(64'd45932479));
		data[4] = (output_size'(64'd43537762));
		data[5] = (output_size'(64'd47309064));
	end
	always @(posedge clk) begin
		result <= data[addr[input_size-1:0]];
	end
endmodule
