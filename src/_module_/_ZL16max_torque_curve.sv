module _ZL16max_torque_curve24
		#( parameter output_size = 64
		)
		( clk
		, addr
		, result
		);
	input clk;
	localparam input_size = 5;
	input[input_size-1:0] addr;
	output reg[output_size-1:0] result;
	logic[output_size-1:0] data[32:0];
	initial begin
		data[0] = (output_size'(64'd2077019340));
		data[1] = (output_size'(64'd3073155264));
		data[2] = (output_size'(64'd2443047032));
		data[3] = (output_size'(64'd3384372699));
		data[4] = (output_size'(64'd2452259909));
		data[5] = (output_size'(64'd3098514493));
		data[6] = (output_size'(64'd3695129761));
		data[7] = (output_size'(64'd3765103891));
		data[8] = (output_size'(64'd3275102628));
		data[9] = (output_size'(64'd3960839760));
		data[10] = (output_size'(64'd2530450681));
		data[11] = (output_size'(64'd3750881271));
		data[12] = (output_size'(64'd2794108456));
		data[13] = (output_size'(64'd3469056215));
		data[14] = (output_size'(64'd2390944425));
		data[15] = (output_size'(64'd3391904904));
		data[16] = (output_size'(64'd2358514154));
		data[17] = (output_size'(64'd3946283322));
		data[18] = (output_size'(64'd2506687355));
		data[19] = (output_size'(64'd3156254246));
		data[20] = (output_size'(64'd3189712581));
		data[21] = (output_size'(64'd3536003776));
		data[22] = (output_size'(64'd3342445864));
		data[23] = (output_size'(64'd2179937453));
		data[24] = (output_size'(64'd3120574919));
		data[25] = (output_size'(64'd2988922209));
		data[26] = (output_size'(64'd2521659253));
		data[27] = (output_size'(64'd2355054867));
		data[28] = (output_size'(64'd2911727731));
		data[29] = (output_size'(64'd2992913396));
		data[30] = (output_size'(64'd2930599069));
		data[31] = (output_size'(64'd3968874305));
	end
	always @(posedge clk) begin
		result <= data[addr[input_size-1:0]];
	end
endmodule
