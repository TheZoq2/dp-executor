module _ZL12Voc_table_dc26
		#( parameter output_size = 64
		)
		( clk
		, addr
		, result
		);
	input clk;
	localparam input_size = 6;
	input[input_size-1:0] addr;
	output reg[output_size-1:0] result;
	logic[output_size-1:0] data[51:0];
	initial begin
		data[0] = (output_size'(64'd2303176212));
		data[1] = (output_size'(64'd3119777264));
		data[2] = (output_size'(64'd3131698877));
		data[3] = (output_size'(64'd2390161556));
		data[4] = (output_size'(64'd2905623483));
		data[5] = (output_size'(64'd2811829115));
		data[6] = (output_size'(64'd2547923269));
		data[7] = (output_size'(64'd2429725137));
		data[8] = (output_size'(64'd2535123210));
		data[9] = (output_size'(64'd3130880379));
		data[10] = (output_size'(64'd2790081745));
		data[11] = (output_size'(64'd2585732560));
		data[12] = (output_size'(64'd2456266457));
		data[13] = (output_size'(64'd2469406868));
		data[14] = (output_size'(64'd3100180969));
		data[15] = (output_size'(64'd2696631392));
		data[16] = (output_size'(64'd3116007522));
		data[17] = (output_size'(64'd2787446754));
		data[18] = (output_size'(64'd2828229165));
		data[19] = (output_size'(64'd2581975208));
		data[20] = (output_size'(64'd2589598411));
		data[21] = (output_size'(64'd3131881135));
		data[22] = (output_size'(64'd3246959094));
		data[23] = (output_size'(64'd3259751659));
		data[24] = (output_size'(64'd2391091873));
		data[25] = (output_size'(64'd2971081655));
		data[26] = (output_size'(64'd2576309698));
		data[27] = (output_size'(64'd2830224953));
		data[28] = (output_size'(64'd3028720219));
		data[29] = (output_size'(64'd2594054871));
		data[30] = (output_size'(64'd2969426007));
		data[31] = (output_size'(64'd3153494902));
		data[32] = (output_size'(64'd2982240741));
		data[33] = (output_size'(64'd2700653035));
		data[34] = (output_size'(64'd2322302360));
		data[35] = (output_size'(64'd2793942058));
		data[36] = (output_size'(64'd2759270247));
		data[37] = (output_size'(64'd2762696757));
		data[38] = (output_size'(64'd2468056309));
		data[39] = (output_size'(64'd3129467223));
		data[40] = (output_size'(64'd2564781854));
		data[41] = (output_size'(64'd3263098618));
		data[42] = (output_size'(64'd2668150689));
		data[43] = (output_size'(64'd3162290916));
		data[44] = (output_size'(64'd2363324398));
		data[45] = (output_size'(64'd2880584370));
		data[46] = (output_size'(64'd2545857725));
		data[47] = (output_size'(64'd2735119172));
		data[48] = (output_size'(64'd2612763561));
		data[49] = (output_size'(64'd2797969654));
		data[50] = (output_size'(64'd3279610183));
	end
	always @(posedge clk) begin
		result <= data[addr[input_size-1:0]];
	end
endmodule
