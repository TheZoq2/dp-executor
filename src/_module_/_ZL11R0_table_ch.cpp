#include "luts.hpp"
Fp<31>const data[] = {
			[0] = Fp<31>::from_raw(41167261ll),
			[1] = Fp<31>::from_raw(45798877ll),
			[2] = Fp<31>::from_raw(41755102ll),
			[3] = Fp<31>::from_raw(45932479ll),
			[4] = Fp<31>::from_raw(43537762ll),
			[5] = Fp<31>::from_raw(47309064ll),
};
Fp<31> _ZL11R0_table_ch31_lut(int i) {
	if (i < 0) {
		return data[0];
	} else if (i >= 6){
		return data[0];
	} else {
	return data[i];
	}
}
