#include "luts.hpp"
Fp<26>const data[] = {
			[0] = Fp<26>::from_raw(2303176212ll),
			[1] = Fp<26>::from_raw(3119777264ll),
			[2] = Fp<26>::from_raw(3131698877ll),
			[3] = Fp<26>::from_raw(2390161556ll),
			[4] = Fp<26>::from_raw(2905623483ll),
			[5] = Fp<26>::from_raw(2811829115ll),
			[6] = Fp<26>::from_raw(2547923269ll),
			[7] = Fp<26>::from_raw(2429725137ll),
			[8] = Fp<26>::from_raw(2535123210ll),
			[9] = Fp<26>::from_raw(3130880379ll),
			[10] = Fp<26>::from_raw(2790081745ll),
			[11] = Fp<26>::from_raw(2585732560ll),
			[12] = Fp<26>::from_raw(2456266457ll),
			[13] = Fp<26>::from_raw(2469406868ll),
			[14] = Fp<26>::from_raw(3100180969ll),
			[15] = Fp<26>::from_raw(2696631392ll),
			[16] = Fp<26>::from_raw(3116007522ll),
			[17] = Fp<26>::from_raw(2787446754ll),
			[18] = Fp<26>::from_raw(2828229165ll),
			[19] = Fp<26>::from_raw(2581975208ll),
			[20] = Fp<26>::from_raw(2589598411ll),
			[21] = Fp<26>::from_raw(3131881135ll),
			[22] = Fp<26>::from_raw(3246959094ll),
			[23] = Fp<26>::from_raw(3259751659ll),
			[24] = Fp<26>::from_raw(2391091873ll),
			[25] = Fp<26>::from_raw(2971081655ll),
			[26] = Fp<26>::from_raw(2576309698ll),
			[27] = Fp<26>::from_raw(2830224953ll),
			[28] = Fp<26>::from_raw(3028720219ll),
			[29] = Fp<26>::from_raw(2594054871ll),
			[30] = Fp<26>::from_raw(2969426007ll),
			[31] = Fp<26>::from_raw(3153494902ll),
			[32] = Fp<26>::from_raw(2982240741ll),
			[33] = Fp<26>::from_raw(2700653035ll),
			[34] = Fp<26>::from_raw(2322302360ll),
			[35] = Fp<26>::from_raw(2793942058ll),
			[36] = Fp<26>::from_raw(2759270247ll),
			[37] = Fp<26>::from_raw(2762696757ll),
			[38] = Fp<26>::from_raw(2468056309ll),
			[39] = Fp<26>::from_raw(3129467223ll),
			[40] = Fp<26>::from_raw(2564781854ll),
			[41] = Fp<26>::from_raw(3263098618ll),
			[42] = Fp<26>::from_raw(2668150689ll),
			[43] = Fp<26>::from_raw(3162290916ll),
			[44] = Fp<26>::from_raw(2363324398ll),
			[45] = Fp<26>::from_raw(2880584370ll),
			[46] = Fp<26>::from_raw(2545857725ll),
			[47] = Fp<26>::from_raw(2735119172ll),
			[48] = Fp<26>::from_raw(2612763561ll),
			[49] = Fp<26>::from_raw(2797969654ll),
			[50] = Fp<26>::from_raw(3279610183ll),
};
Fp<26> _ZL12Voc_table_dc26_lut(int i) {
	if (i < 0) {
		return data[0];
	} else if (i >= 51){
		return data[0];
	} else {
	return data[i];
	}
}
