// Perform fixed point division of 2 signed numbers. The sizes of the ports
// is 1 + INT_BITS + FRAC_BITS to accomodate the sign bit. The pipeline depth
// of the module is
// 2 + LHS_INT_BITS + LHS_FRAC_BITS + OUT_FRAC_BITS + RHS_FRAC_BITS - LHS_FRAC_BITS
// =
// 2 + LHS_INT_BITS + OUT_FRAC_BITS + RHS_FRAC_BITS
// Where 2 comes from the sign handling
module divider
   #( parameter LHS_INT_BITS = 8
    , parameter LHS_FRAC_BITS = 8
    , parameter RHS_INT_BITS = 8
    , parameter RHS_FRAC_BITS = 8
    , parameter OUT_INT_BITS = 8
    , parameter OUT_FRAC_BITS = 8
    , localparam LHS_SIZE = LHS_INT_BITS + LHS_FRAC_BITS
    , localparam RHS_SIZE = RHS_INT_BITS + RHS_FRAC_BITS
    , localparam SHIFT_AMOUNT = OUT_FRAC_BITS + RHS_FRAC_BITS - LHS_FRAC_BITS
    , localparam SHIFTED_SIZE = LHS_INT_BITS + LHS_FRAC_BITS + SHIFT_AMOUNT
    )
    ( input clk
    , input signed[LHS_INT_BITS+LHS_FRAC_BITS:0] lhs_signed
    , input signed[RHS_INT_BITS+RHS_FRAC_BITS:0] rhs_signed
    , output reg[SHIFTED_SIZE:0] out_signed
    );

    localparam NUM_DIVIDER_STAGES = SHIFTED_SIZE;

    // Transform to and from signed values

    reg[LHS_SIZE-1:0] lhs;
    reg[RHS_SIZE-1:0] rhs;
    reg[NUM_DIVIDER_STAGES:0] output_negative;

    wire lhs_positive = lhs_signed >= 0;
    wire rhs_positive = rhs_signed >= 0;

    always @(posedge clk) begin

        // Verilog doesn't allow `(-x)[1:0]` for whatever reason, so we'll
        // have to write out the inverse + 1 expression
        lhs <= (lhs_positive ? lhs_signed[LHS_SIZE-1:0] : ~lhs_signed[LHS_SIZE-1:0] + 1);
        rhs <= (rhs_positive ? rhs_signed[RHS_SIZE-1:0] : ~rhs_signed[RHS_SIZE-1:0] + 1);

        output_negative[0] <= lhs_positive ^ rhs_positive;
    end

    // Propagate the output sign through the pipeline
    generate
        genvar i;
        for(i = 1; i <= NUM_DIVIDER_STAGES; i = i + 1) begin
            always @(posedge clk) begin
                output_negative[i] <= output_negative[i-1];
            end
        end
    endgenerate

    wire[SHIFTED_SIZE-1:0] shifted_lhs = {lhs, {SHIFT_AMOUNT{1'b0}}};

    generate
        genvar stage;
        for (stage = 0; stage < NUM_DIVIDER_STAGES; stage = stage + 1) begin: block
            reg[stage:0] remainder;
            reg[stage:0] result;
            reg[SHIFTED_SIZE-1:0] lhs_buf;
            reg[RHS_SIZE-1:0] rhs_buf;

            if(stage == 0) begin
                wire val;
                // First stage produces 1 bit and needs 1 sign bit
                wire signed[RHS_SIZE:0] sub;

                assign val = shifted_lhs[SHIFTED_SIZE-1];
                assign sub = {{{{RHS_SIZE{1'b0}}}, val} - rhs};

                always @(posedge clk) begin
                    // Buffer inputs
                    lhs_buf <= shifted_lhs;
                    rhs_buf <= rhs;

                    // Compute and store remainder and result
                    if (sub < 0) begin
                        remainder <= val;
                        result <= 0;
                    end
                    else begin
                        // Remove sign bit, we know the value is positive
                        remainder <= sub[0];
                        result <= 1;
                    end
                end
            end else begin
                localparam SUB_SIZE = (RHS_SIZE > stage) ? RHS_SIZE : stage;
                wire[stage:0] val;
                wire signed[SUB_SIZE:0] sub;

                wire[stage-1:0] prev_remainder = block[stage-1].remainder;
                wire[SHIFTED_SIZE-1:0] prev_lhs = block[stage-1].lhs_buf;
                wire[RHS_SIZE-1:0] prev_rhs = block[stage-1].rhs_buf;
                wire prev_bit = prev_lhs[SHIFTED_SIZE - stage - 1];

                assign val = { prev_remainder, prev_bit };

                // Depending on if the remainder or the rhs is the largest, we
                // need to sign extend one of the two
                if(RHS_SIZE == stage) begin
                    assign sub = {val - prev_rhs};
                end
                else if(RHS_SIZE > stage) begin
                    assign sub = {{{{RHS_SIZE-stage{1'b0}}}, val} - prev_rhs};
                end else begin
                    assign sub = {val - {{{stage-RHS_SIZE{1'b0}}}, prev_rhs}};
                end

                always @(posedge clk) begin
                    rhs_buf <= prev_rhs;
                    lhs_buf <= prev_lhs;
                    if (sub < 0) begin
                        remainder <= val;
                        result <= {block[stage-1].result, 1'b0};
                    end
                    else begin
                        // Remove the sign bit since we know the value is
                        // positive
                        remainder <= sub[stage:0];
                        result <= {block[stage-1].result, 1'b1};
                    end
                end
            end
        end
    endgenerate

    wire [NUM_DIVIDER_STAGES-1:0] out_unsigned;
    assign out_unsigned = block[NUM_DIVIDER_STAGES-1].result;

    always @(posedge clk) begin
        if(output_negative[NUM_DIVIDER_STAGES]) begin
            out_signed <= {1'b1, ~out_unsigned} + 1;
        end else begin
            out_signed <= {1'b0, out_unsigned};
        end
    end
endmodule
