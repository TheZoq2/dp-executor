module _ZL11R0_table_dc31
		#( parameter output_size = 64
		)
		( clk
		, addr
		, result
		);
	input clk;
	localparam input_size = 3;
	input[input_size-1:0] addr;
	output reg[output_size-1:0] result;
	logic[output_size-1:0] data[6:0];
	initial begin
		data[0] = (output_size'(64'd42606075));
		data[1] = (output_size'(64'd47727068));
		data[2] = (output_size'(64'd46788240));
		data[3] = (output_size'(64'd45695210));
		data[4] = (output_size'(64'd43856018));
		data[5] = (output_size'(64'd48039209));
	end
	always @(posedge clk) begin
		result <= data[addr[input_size-1:0]];
	end
endmodule
