#include "luts.hpp"
Fp<31>const data[] = {
			[0] = Fp<31>::from_raw(42606075ll),
			[1] = Fp<31>::from_raw(47727068ll),
			[2] = Fp<31>::from_raw(46788240ll),
			[3] = Fp<31>::from_raw(45695210ll),
			[4] = Fp<31>::from_raw(43856018ll),
			[5] = Fp<31>::from_raw(48039209ll),
};
Fp<31> _ZL11R0_table_dc31_lut(int i) {
	if (i < 0) {
		return data[0];
	} else if (i >= 6){
		return data[0];
	} else {
	return data[i];
	}
}
