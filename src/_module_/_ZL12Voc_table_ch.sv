module _ZL12Voc_table_ch26
		#( parameter output_size = 64
		)
		( clk
		, addr
		, result
		);
	input clk;
	localparam input_size = 6;
	input[input_size-1:0] addr;
	output reg[output_size-1:0] result;
	logic[output_size-1:0] data[51:0];
	initial begin
		data[0] = (output_size'(64'd2303176212));
		data[1] = (output_size'(64'd2562069804));
		data[2] = (output_size'(64'd2515274933));
		data[3] = (output_size'(64'd3212278224));
		data[4] = (output_size'(64'd2909883824));
		data[5] = (output_size'(64'd2432599426));
		data[6] = (output_size'(64'd2812833650));
		data[7] = (output_size'(64'd3105518222));
		data[8] = (output_size'(64'd2820967540));
		data[9] = (output_size'(64'd2576125625));
		data[10] = (output_size'(64'd2565256407));
		data[11] = (output_size'(64'd2477737220));
		data[12] = (output_size'(64'd2802670645));
		data[13] = (output_size'(64'd2430089952));
		data[14] = (output_size'(64'd2709179947));
		data[15] = (output_size'(64'd2760819120));
		data[16] = (output_size'(64'd2906036983));
		data[17] = (output_size'(64'd2554740012));
		data[18] = (output_size'(64'd2999685317));
		data[19] = (output_size'(64'd2994996176));
		data[20] = (output_size'(64'd2912499022));
		data[21] = (output_size'(64'd2306576539));
		data[22] = (output_size'(64'd2341203494));
		data[23] = (output_size'(64'd3116736371));
		data[24] = (output_size'(64'd2615236225));
		data[25] = (output_size'(64'd3256614380));
		data[26] = (output_size'(64'd2483554225));
		data[27] = (output_size'(64'd2509292285));
		data[28] = (output_size'(64'd3204324454));
		data[29] = (output_size'(64'd2491215539));
		data[30] = (output_size'(64'd3189662042));
		data[31] = (output_size'(64'd2805084531));
		data[32] = (output_size'(64'd3259924370));
		data[33] = (output_size'(64'd3018789566));
		data[34] = (output_size'(64'd2904592631));
		data[35] = (output_size'(64'd2980586033));
		data[36] = (output_size'(64'd2713305728));
		data[37] = (output_size'(64'd3100730153));
		data[38] = (output_size'(64'd2320726313));
		data[39] = (output_size'(64'd3224100817));
		data[40] = (output_size'(64'd3164727143));
		data[41] = (output_size'(64'd2872035020));
		data[42] = (output_size'(64'd2996205261));
		data[43] = (output_size'(64'd2998798347));
		data[44] = (output_size'(64'd2505020630));
		data[45] = (output_size'(64'd2332650708));
		data[46] = (output_size'(64'd3069445245));
		data[47] = (output_size'(64'd2541477685));
		data[48] = (output_size'(64'd2472843836));
		data[49] = (output_size'(64'd2316006359));
		data[50] = (output_size'(64'd3283636715));
	end
	always @(posedge clk) begin
		result <= data[addr[input_size-1:0]];
	end
endmodule
