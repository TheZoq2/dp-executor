// Computes
//
// int i = trunc( (xp-x_min)/dx );
// float x_offset = ((xp-x_min) - i * dx);
// return lut_y(i).as_float() + (lut_y(i+1)-lut_y(i)).as_float() * x_offset/dx;
//
// `/dx` is too expensive, so instead, `(xp-xmin)` are multiplied by `alpha`
// to allow shift by `n`
//
// We want
// x / dx = x*alpha / (dx*alpha) where alpha scales dx to the closest larger
// power of 2
//
// The closest larger power of 2 is `2 ** ceil(log2(dx))`
//
// We want alpha*dx = 2 ** ceil(log2(dx))
// which means
// alpha = (2 ** ceil(log2(dx))) / dx
//
// dx, alpha, x_min, x_max, lut1_output, lut2_output should have its fixed
// point at INPUT_FP

module interp1d
    #(parameter signed INPUT_INT_BITS = -1
    , parameter signed INPUT_FP = -1
    , parameter signed OUTPUT_SIZE = -1
    , parameter signed OUTPUT_FP = -1
    , parameter signed LUT_ADDRESS_SIZE = -1
    , parameter signed LUT_INT_BITS = -1
    , parameter signed ALPHA = -1
    , parameter signed DX = -1
    , parameter signed LOG_DX = -1
    , parameter signed X_MIN = -1
    , localparam signed LUT_OUTPUT_SIZE = LUT_INT_BITS + INPUT_FP + 1
    , localparam signed INPUT_SIZE = INPUT_INT_BITS + INPUT_FP + 1
    )
    ( input clk
    , input[INPUT_SIZE-1:0] x_s0
    , output[LUT_ADDRESS_SIZE-1:0] lut1_addr
    , input[LUT_OUTPUT_SIZE-1:0] lut1_output
    , output[LUT_ADDRESS_SIZE-1:0] lut2_addr
    , input[LUT_OUTPUT_SIZE-1:0] lut2_output
    , output[OUTPUT_SIZE-1:0] out
    );

    // Alpha is aligned the same way as x, and is between 0.5 and 2 which
    // gives the first + 1. Second +1 comes from sign bit
    localparam signed X_SCALED_SIZE = INPUT_INT_BITS + INPUT_FP * 2 + 1 + 1;
    // X_SCALED_SIZE divided by dx without fractional bits 
    localparam signed I_SIZE = INPUT_INT_BITS - LOG_DX + 1 + 1;

    // Debug signals
    // integer _debug_scaled_cutoff;
    // integer _debug_x_min;
    // integer _debug_x_scaled_size;
    // integer _dx;
    // integer _debug_log_dx_fp;
    // assign _debug_scaled_cutoff = LOG_DX + INPUT_FP*2;
    // assign _debug_x_min = X_MIN;
    // assign _dx = DX;
    // assign _debug_x_scaled_size = X_SCALED_SIZE;
    // assign _debug_log_dx_fp = LOG_DX + INPUT_FP;

    // Fixed points:
    // x: INPUT_FP
    // x_sub: INPUT_fp
    // x_scaled: INPUT_FP*2
    // i: 0
    // x_offset: INPUT_FP
    // f_i: INPUT_FP
    // slope: INPUT_FP

    // Stage 1
    reg[INPUT_SIZE-1:0] x_sub_s1;
    reg[INPUT_SIZE-1:0] x_s1;

    always @(posedge clk) begin
        x_sub_s1 <= x_s0 - X_MIN;
        x_s1 <= x_s0;
    end

    // Stage 2
    reg[INPUT_SIZE-1:0] x_s2;
    reg[INPUT_SIZE-1:0] x_sub_s2;

    wire[X_SCALED_SIZE-1:0] x_scaled_s1;
    assign x_scaled_s1 = x_sub_s1 * ALPHA;
    reg[I_SIZE-1:0] i_s2;
    always @(posedge clk) begin
        i_s2 <= x_scaled_s1[X_SCALED_SIZE-1:LOG_DX + INPUT_FP*2];
        x_s2 <= x_s1;
        x_sub_s2 <= x_sub_s1;
    end

    // Stage 3 (LUT)
    assign lut1_addr = i_s2;
    assign lut2_addr = i_s2 + 1;

    reg[INPUT_SIZE-1:0] x_offset_s3;
    always @(posedge clk) begin
        // NOTE: i_shifted has 0 frac bits, DX has INPUT_FP. Multiplication
        // gives correctly aligned result
        x_offset_s3 <= x_sub_s2 - i_s2 * DX;
    end

    // Stage 4
    reg[LUT_OUTPUT_SIZE-1:0] slope_s4;
    reg[LUT_OUTPUT_SIZE-1:0] f_i_s4;
    reg[X_SCALED_SIZE-1:0] x_offset_scaled_s4;
    wire[X_SCALED_SIZE-1 + INPUT_FP:0] x_offset_scaled_s4_buff;
    assign x_offset_scaled_s4_buff = x_offset_s3 * ALPHA;
    always @(posedge clk) begin
        x_offset_scaled_s4 <= x_offset_scaled_s4_buff[X_SCALED_SIZE-1:LOG_DX + INPUT_FP];
        slope_s4 <= lut2_output - lut1_output;
        f_i_s4 <= lut1_output;
    end

    localparam signed RESULT_BUFFER_SIZE = 1 + LUT_INT_BITS*2 + INPUT_FP*2;
    reg[RESULT_BUFFER_SIZE-1:0] result;
    always @(posedge clk) begin
        result <= {{LUT_INT_BITS{f_i_s4[LUT_OUTPUT_SIZE-1]}}, f_i_s4, {INPUT_FP{1'b0}}} + slope_s4 * x_offset_scaled_s4;
    end

    localparam signed RESULT_FP = INPUT_FP * 2;
    localparam signed RESULT_END = RESULT_FP - OUTPUT_FP;
    localparam signed RESULT_START = RESULT_END + OUTPUT_SIZE;
    assign out = result[RESULT_START-1:RESULT_END];
endmodule
