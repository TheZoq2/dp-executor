#include "luts.hpp"
Fp<24>const data[] = {
			[0] = Fp<24>::from_raw(2077019340ll),
			[1] = Fp<24>::from_raw(3073155264ll),
			[2] = Fp<24>::from_raw(2443047032ll),
			[3] = Fp<24>::from_raw(3384372699ll),
			[4] = Fp<24>::from_raw(2452259909ll),
			[5] = Fp<24>::from_raw(3098514493ll),
			[6] = Fp<24>::from_raw(3695129761ll),
			[7] = Fp<24>::from_raw(3765103891ll),
			[8] = Fp<24>::from_raw(3275102628ll),
			[9] = Fp<24>::from_raw(3960839760ll),
			[10] = Fp<24>::from_raw(2530450681ll),
			[11] = Fp<24>::from_raw(3750881271ll),
			[12] = Fp<24>::from_raw(2794108456ll),
			[13] = Fp<24>::from_raw(3469056215ll),
			[14] = Fp<24>::from_raw(2390944425ll),
			[15] = Fp<24>::from_raw(3391904904ll),
			[16] = Fp<24>::from_raw(2358514154ll),
			[17] = Fp<24>::from_raw(3946283322ll),
			[18] = Fp<24>::from_raw(2506687355ll),
			[19] = Fp<24>::from_raw(3156254246ll),
			[20] = Fp<24>::from_raw(3189712581ll),
			[21] = Fp<24>::from_raw(3536003776ll),
			[22] = Fp<24>::from_raw(3342445864ll),
			[23] = Fp<24>::from_raw(2179937453ll),
			[24] = Fp<24>::from_raw(3120574919ll),
			[25] = Fp<24>::from_raw(2988922209ll),
			[26] = Fp<24>::from_raw(2521659253ll),
			[27] = Fp<24>::from_raw(2355054867ll),
			[28] = Fp<24>::from_raw(2911727731ll),
			[29] = Fp<24>::from_raw(2992913396ll),
			[30] = Fp<24>::from_raw(2930599069ll),
			[31] = Fp<24>::from_raw(3968874305ll),
};
Fp<24> _ZL16max_torque_curve24_lut(int i) {
	if (i < 0) {
		return data[0];
	} else if (i >= 32){
		return data[0];
	} else {
	return data[i];
	}
}
