module _ZL11gear_ratios29
		#( parameter output_size = 64
		)
		( clk
		, addr
		, result
		);
	input clk;
	localparam input_size = 3;
	input[input_size-1:0] addr;
	output reg[output_size-1:0] result;
	logic[output_size-1:0] data[6:0];
	initial begin
		data[0] = (output_size'(64'd359703511));
		data[1] = (output_size'(64'd536960723));
		data[2] = (output_size'(64'd1229339881));
		data[3] = (output_size'(64'd1429602473));
		data[4] = (output_size'(64'd1007462749));
		data[5] = (output_size'(64'd2415919104));
	end
	always @(posedge clk) begin
		result <= data[addr[input_size-1:0]];
	end
endmodule
