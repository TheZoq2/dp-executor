#include "luts.hpp"
Fp<26>const data[] = {
			[0] = Fp<26>::from_raw(2303176212ll),
			[1] = Fp<26>::from_raw(2562069804ll),
			[2] = Fp<26>::from_raw(2515274933ll),
			[3] = Fp<26>::from_raw(3212278224ll),
			[4] = Fp<26>::from_raw(2909883824ll),
			[5] = Fp<26>::from_raw(2432599426ll),
			[6] = Fp<26>::from_raw(2812833650ll),
			[7] = Fp<26>::from_raw(3105518222ll),
			[8] = Fp<26>::from_raw(2820967540ll),
			[9] = Fp<26>::from_raw(2576125625ll),
			[10] = Fp<26>::from_raw(2565256407ll),
			[11] = Fp<26>::from_raw(2477737220ll),
			[12] = Fp<26>::from_raw(2802670645ll),
			[13] = Fp<26>::from_raw(2430089952ll),
			[14] = Fp<26>::from_raw(2709179947ll),
			[15] = Fp<26>::from_raw(2760819120ll),
			[16] = Fp<26>::from_raw(2906036983ll),
			[17] = Fp<26>::from_raw(2554740012ll),
			[18] = Fp<26>::from_raw(2999685317ll),
			[19] = Fp<26>::from_raw(2994996176ll),
			[20] = Fp<26>::from_raw(2912499022ll),
			[21] = Fp<26>::from_raw(2306576539ll),
			[22] = Fp<26>::from_raw(2341203494ll),
			[23] = Fp<26>::from_raw(3116736371ll),
			[24] = Fp<26>::from_raw(2615236225ll),
			[25] = Fp<26>::from_raw(3256614380ll),
			[26] = Fp<26>::from_raw(2483554225ll),
			[27] = Fp<26>::from_raw(2509292285ll),
			[28] = Fp<26>::from_raw(3204324454ll),
			[29] = Fp<26>::from_raw(2491215539ll),
			[30] = Fp<26>::from_raw(3189662042ll),
			[31] = Fp<26>::from_raw(2805084531ll),
			[32] = Fp<26>::from_raw(3259924370ll),
			[33] = Fp<26>::from_raw(3018789566ll),
			[34] = Fp<26>::from_raw(2904592631ll),
			[35] = Fp<26>::from_raw(2980586033ll),
			[36] = Fp<26>::from_raw(2713305728ll),
			[37] = Fp<26>::from_raw(3100730153ll),
			[38] = Fp<26>::from_raw(2320726313ll),
			[39] = Fp<26>::from_raw(3224100817ll),
			[40] = Fp<26>::from_raw(3164727143ll),
			[41] = Fp<26>::from_raw(2872035020ll),
			[42] = Fp<26>::from_raw(2996205261ll),
			[43] = Fp<26>::from_raw(2998798347ll),
			[44] = Fp<26>::from_raw(2505020630ll),
			[45] = Fp<26>::from_raw(2332650708ll),
			[46] = Fp<26>::from_raw(3069445245ll),
			[47] = Fp<26>::from_raw(2541477685ll),
			[48] = Fp<26>::from_raw(2472843836ll),
			[49] = Fp<26>::from_raw(2316006359ll),
			[50] = Fp<26>::from_raw(3283636715ll),
};
Fp<26> _ZL12Voc_table_ch26_lut(int i) {
	if (i < 0) {
		return data[0];
	} else if (i >= 51){
		return data[0];
	} else {
	return data[i];
	}
}
