module sqrt_33_21_26 (clk, i, o);
    input clk;
    input[32:0] i;
    output[31:0] o;
    wire[63:0] s0Input = {i, 31'd0};
    wire[63:0] s0Result = 0;
    reg[63:0] s1Input;
    reg[63:0] s1Result;
    wire[63:0] s1Sub = s0Input - (s0Result + (1 << 62));
    always @(posedge clk) begin
        if ($signed(s1Sub) >= 0) begin
            s1Input <= s1Sub;
            s1Result <= (s0Result >> 1) + (1 << 62);
        end else begin
            s1Input <= s0Input;
            s1Result <= (s0Result >> 1);
        end
    end
    reg[63:0] s2Input;
    reg[63:0] s2Result;
    wire[63:0] s2Sub = s1Input - (s1Result + (1 << 60));
    always @(posedge clk) begin
        if ($signed(s2Sub) >= 0) begin
            s2Input <= s2Sub;
            s2Result <= (s1Result >> 1) + (1 << 60);
        end else begin
            s2Input <= s1Input;
            s2Result <= (s1Result >> 1);
        end
    end
    reg[63:0] s3Input;
    reg[63:0] s3Result;
    wire[63:0] s3Sub = s2Input - (s2Result + (1 << 58));
    always @(posedge clk) begin
        if ($signed(s3Sub) >= 0) begin
            s3Input <= s3Sub;
            s3Result <= (s2Result >> 1) + (1 << 58);
        end else begin
            s3Input <= s2Input;
            s3Result <= (s2Result >> 1);
        end
    end
    reg[63:0] s4Input;
    reg[63:0] s4Result;
    wire[63:0] s4Sub = s3Input - (s3Result + (1 << 56));
    always @(posedge clk) begin
        if ($signed(s4Sub) >= 0) begin
            s4Input <= s4Sub;
            s4Result <= (s3Result >> 1) + (1 << 56);
        end else begin
            s4Input <= s3Input;
            s4Result <= (s3Result >> 1);
        end
    end
    reg[63:0] s5Input;
    reg[63:0] s5Result;
    wire[63:0] s5Sub = s4Input - (s4Result + (1 << 54));
    always @(posedge clk) begin
        if ($signed(s5Sub) >= 0) begin
            s5Input <= s5Sub;
            s5Result <= (s4Result >> 1) + (1 << 54);
        end else begin
            s5Input <= s4Input;
            s5Result <= (s4Result >> 1);
        end
    end
    reg[63:0] s6Input;
    reg[63:0] s6Result;
    wire[63:0] s6Sub = s5Input - (s5Result + (1 << 52));
    always @(posedge clk) begin
        if ($signed(s6Sub) >= 0) begin
            s6Input <= s6Sub;
            s6Result <= (s5Result >> 1) + (1 << 52);
        end else begin
            s6Input <= s5Input;
            s6Result <= (s5Result >> 1);
        end
    end
    reg[63:0] s7Input;
    reg[63:0] s7Result;
    wire[63:0] s7Sub = s6Input - (s6Result + (1 << 50));
    always @(posedge clk) begin
        if ($signed(s7Sub) >= 0) begin
            s7Input <= s7Sub;
            s7Result <= (s6Result >> 1) + (1 << 50);
        end else begin
            s7Input <= s6Input;
            s7Result <= (s6Result >> 1);
        end
    end
    reg[63:0] s8Input;
    reg[63:0] s8Result;
    wire[63:0] s8Sub = s7Input - (s7Result + (1 << 48));
    always @(posedge clk) begin
        if ($signed(s8Sub) >= 0) begin
            s8Input <= s8Sub;
            s8Result <= (s7Result >> 1) + (1 << 48);
        end else begin
            s8Input <= s7Input;
            s8Result <= (s7Result >> 1);
        end
    end
    reg[63:0] s9Input;
    reg[63:0] s9Result;
    wire[63:0] s9Sub = s8Input - (s8Result + (1 << 46));
    always @(posedge clk) begin
        if ($signed(s9Sub) >= 0) begin
            s9Input <= s9Sub;
            s9Result <= (s8Result >> 1) + (1 << 46);
        end else begin
            s9Input <= s8Input;
            s9Result <= (s8Result >> 1);
        end
    end
    reg[63:0] s10Input;
    reg[63:0] s10Result;
    wire[63:0] s10Sub = s9Input - (s9Result + (1 << 44));
    always @(posedge clk) begin
        if ($signed(s10Sub) >= 0) begin
            s10Input <= s10Sub;
            s10Result <= (s9Result >> 1) + (1 << 44);
        end else begin
            s10Input <= s9Input;
            s10Result <= (s9Result >> 1);
        end
    end
    reg[63:0] s11Input;
    reg[63:0] s11Result;
    wire[63:0] s11Sub = s10Input - (s10Result + (1 << 42));
    always @(posedge clk) begin
        if ($signed(s11Sub) >= 0) begin
            s11Input <= s11Sub;
            s11Result <= (s10Result >> 1) + (1 << 42);
        end else begin
            s11Input <= s10Input;
            s11Result <= (s10Result >> 1);
        end
    end
    reg[63:0] s12Input;
    reg[63:0] s12Result;
    wire[63:0] s12Sub = s11Input - (s11Result + (1 << 40));
    always @(posedge clk) begin
        if ($signed(s12Sub) >= 0) begin
            s12Input <= s12Sub;
            s12Result <= (s11Result >> 1) + (1 << 40);
        end else begin
            s12Input <= s11Input;
            s12Result <= (s11Result >> 1);
        end
    end
    reg[63:0] s13Input;
    reg[63:0] s13Result;
    wire[63:0] s13Sub = s12Input - (s12Result + (1 << 38));
    always @(posedge clk) begin
        if ($signed(s13Sub) >= 0) begin
            s13Input <= s13Sub;
            s13Result <= (s12Result >> 1) + (1 << 38);
        end else begin
            s13Input <= s12Input;
            s13Result <= (s12Result >> 1);
        end
    end
    reg[63:0] s14Input;
    reg[63:0] s14Result;
    wire[63:0] s14Sub = s13Input - (s13Result + (1 << 36));
    always @(posedge clk) begin
        if ($signed(s14Sub) >= 0) begin
            s14Input <= s14Sub;
            s14Result <= (s13Result >> 1) + (1 << 36);
        end else begin
            s14Input <= s13Input;
            s14Result <= (s13Result >> 1);
        end
    end
    reg[63:0] s15Input;
    reg[63:0] s15Result;
    wire[63:0] s15Sub = s14Input - (s14Result + (1 << 34));
    always @(posedge clk) begin
        if ($signed(s15Sub) >= 0) begin
            s15Input <= s15Sub;
            s15Result <= (s14Result >> 1) + (1 << 34);
        end else begin
            s15Input <= s14Input;
            s15Result <= (s14Result >> 1);
        end
    end
    reg[63:0] s16Input;
    reg[63:0] s16Result;
    wire[63:0] s16Sub = s15Input - (s15Result + (1 << 32));
    always @(posedge clk) begin
        if ($signed(s16Sub) >= 0) begin
            s16Input <= s16Sub;
            s16Result <= (s15Result >> 1) + (1 << 32);
        end else begin
            s16Input <= s15Input;
            s16Result <= (s15Result >> 1);
        end
    end
    reg[63:0] s17Input;
    reg[63:0] s17Result;
    wire[63:0] s17Sub = s16Input - (s16Result + (1 << 30));
    always @(posedge clk) begin
        if ($signed(s17Sub) >= 0) begin
            s17Input <= s17Sub;
            s17Result <= (s16Result >> 1) + (1 << 30);
        end else begin
            s17Input <= s16Input;
            s17Result <= (s16Result >> 1);
        end
    end
    reg[63:0] s18Input;
    reg[63:0] s18Result;
    wire[63:0] s18Sub = s17Input - (s17Result + (1 << 28));
    always @(posedge clk) begin
        if ($signed(s18Sub) >= 0) begin
            s18Input <= s18Sub;
            s18Result <= (s17Result >> 1) + (1 << 28);
        end else begin
            s18Input <= s17Input;
            s18Result <= (s17Result >> 1);
        end
    end
    reg[63:0] s19Input;
    reg[63:0] s19Result;
    wire[63:0] s19Sub = s18Input - (s18Result + (1 << 26));
    always @(posedge clk) begin
        if ($signed(s19Sub) >= 0) begin
            s19Input <= s19Sub;
            s19Result <= (s18Result >> 1) + (1 << 26);
        end else begin
            s19Input <= s18Input;
            s19Result <= (s18Result >> 1);
        end
    end
    reg[63:0] s20Input;
    reg[63:0] s20Result;
    wire[63:0] s20Sub = s19Input - (s19Result + (1 << 24));
    always @(posedge clk) begin
        if ($signed(s20Sub) >= 0) begin
            s20Input <= s20Sub;
            s20Result <= (s19Result >> 1) + (1 << 24);
        end else begin
            s20Input <= s19Input;
            s20Result <= (s19Result >> 1);
        end
    end
    reg[63:0] s21Input;
    reg[63:0] s21Result;
    wire[63:0] s21Sub = s20Input - (s20Result + (1 << 22));
    always @(posedge clk) begin
        if ($signed(s21Sub) >= 0) begin
            s21Input <= s21Sub;
            s21Result <= (s20Result >> 1) + (1 << 22);
        end else begin
            s21Input <= s20Input;
            s21Result <= (s20Result >> 1);
        end
    end
    reg[63:0] s22Input;
    reg[63:0] s22Result;
    wire[63:0] s22Sub = s21Input - (s21Result + (1 << 20));
    always @(posedge clk) begin
        if ($signed(s22Sub) >= 0) begin
            s22Input <= s22Sub;
            s22Result <= (s21Result >> 1) + (1 << 20);
        end else begin
            s22Input <= s21Input;
            s22Result <= (s21Result >> 1);
        end
    end
    reg[63:0] s23Input;
    reg[63:0] s23Result;
    wire[63:0] s23Sub = s22Input - (s22Result + (1 << 18));
    always @(posedge clk) begin
        if ($signed(s23Sub) >= 0) begin
            s23Input <= s23Sub;
            s23Result <= (s22Result >> 1) + (1 << 18);
        end else begin
            s23Input <= s22Input;
            s23Result <= (s22Result >> 1);
        end
    end
    reg[63:0] s24Input;
    reg[63:0] s24Result;
    wire[63:0] s24Sub = s23Input - (s23Result + (1 << 16));
    always @(posedge clk) begin
        if ($signed(s24Sub) >= 0) begin
            s24Input <= s24Sub;
            s24Result <= (s23Result >> 1) + (1 << 16);
        end else begin
            s24Input <= s23Input;
            s24Result <= (s23Result >> 1);
        end
    end
    reg[63:0] s25Input;
    reg[63:0] s25Result;
    wire[63:0] s25Sub = s24Input - (s24Result + (1 << 14));
    always @(posedge clk) begin
        if ($signed(s25Sub) >= 0) begin
            s25Input <= s25Sub;
            s25Result <= (s24Result >> 1) + (1 << 14);
        end else begin
            s25Input <= s24Input;
            s25Result <= (s24Result >> 1);
        end
    end
    reg[63:0] s26Input;
    reg[63:0] s26Result;
    wire[63:0] s26Sub = s25Input - (s25Result + (1 << 12));
    always @(posedge clk) begin
        if ($signed(s26Sub) >= 0) begin
            s26Input <= s26Sub;
            s26Result <= (s25Result >> 1) + (1 << 12);
        end else begin
            s26Input <= s25Input;
            s26Result <= (s25Result >> 1);
        end
    end
    reg[63:0] s27Input;
    reg[63:0] s27Result;
    wire[63:0] s27Sub = s26Input - (s26Result + (1 << 10));
    always @(posedge clk) begin
        if ($signed(s27Sub) >= 0) begin
            s27Input <= s27Sub;
            s27Result <= (s26Result >> 1) + (1 << 10);
        end else begin
            s27Input <= s26Input;
            s27Result <= (s26Result >> 1);
        end
    end
    reg[63:0] s28Input;
    reg[63:0] s28Result;
    wire[63:0] s28Sub = s27Input - (s27Result + (1 << 8));
    always @(posedge clk) begin
        if ($signed(s28Sub) >= 0) begin
            s28Input <= s28Sub;
            s28Result <= (s27Result >> 1) + (1 << 8);
        end else begin
            s28Input <= s27Input;
            s28Result <= (s27Result >> 1);
        end
    end
    reg[63:0] s29Input;
    reg[63:0] s29Result;
    wire[63:0] s29Sub = s28Input - (s28Result + (1 << 6));
    always @(posedge clk) begin
        if ($signed(s29Sub) >= 0) begin
            s29Input <= s29Sub;
            s29Result <= (s28Result >> 1) + (1 << 6);
        end else begin
            s29Input <= s28Input;
            s29Result <= (s28Result >> 1);
        end
    end
    reg[63:0] s30Input;
    reg[63:0] s30Result;
    wire[63:0] s30Sub = s29Input - (s29Result + (1 << 4));
    always @(posedge clk) begin
        if ($signed(s30Sub) >= 0) begin
            s30Input <= s30Sub;
            s30Result <= (s29Result >> 1) + (1 << 4);
        end else begin
            s30Input <= s29Input;
            s30Result <= (s29Result >> 1);
        end
    end
    reg[63:0] s31Input;
    reg[63:0] s31Result;
    wire[63:0] s31Sub = s30Input - (s30Result + (1 << 2));
    always @(posedge clk) begin
        if ($signed(s31Sub) >= 0) begin
            s31Input <= s31Sub;
            s31Result <= (s30Result >> 1) + (1 << 2);
        end else begin
            s31Input <= s30Input;
            s31Result <= (s30Result >> 1);
        end
    end
    reg[63:0] s32Input;
    reg[63:0] s32Result;
    wire[63:0] s32Sub = s31Input - (s31Result + (1 << 0));
    always @(posedge clk) begin
        if ($signed(s32Sub) >= 0) begin
            s32Input <= s32Sub;
            s32Result <= (s31Result >> 1) + (1 << 0);
        end else begin
            s32Input <= s31Input;
            s32Result <= (s31Result >> 1);
        end
    end
    assign o = s32Result;
endmodule
