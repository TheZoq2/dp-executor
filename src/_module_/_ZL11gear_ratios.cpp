#include "luts.hpp"
Fp<29>const data[] = {
			[0] = Fp<29>::from_raw(359703511ll),
			[1] = Fp<29>::from_raw(536960723ll),
			[2] = Fp<29>::from_raw(1229339881ll),
			[3] = Fp<29>::from_raw(1429602473ll),
			[4] = Fp<29>::from_raw(1007462749ll),
			[5] = Fp<29>::from_raw(2415919104ll),
};
Fp<29> _ZL11gear_ratios29_lut(int i) {
	if (i < 0) {
		return data[0];
	} else if (i >= 6){
		return data[0];
	} else {
	return data[i];
	}
}
