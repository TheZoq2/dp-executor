#pragma once
#include "luts.hpp"
#include <fixedpoint.hpp>
#include <functions.hpp>
#include <interpolation_impl.hpp>
struct _module__output {
	Fp<19> __0;
	Fp<18> __1;
	Fp<28> __2;
	Fp<18> __3;
	Fp<22> __4;
	Fp<20> __5;
	Fp<13> __6;
	bool __7;
};
struct _module__input {
	Fp<23> u1;
	Fp<26> u2;
	Fp<28> dt;
	Fp<31> x2;
	Fp<21> x1;
	Fp<29> u3;
};
_module__output _module_ (_module__input __input);
