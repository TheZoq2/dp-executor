module top(input clk, output done);
    reg rst = 1;

    always @(posedge clk) begin
        rst <= 0;
    end
    e_proj_main_main main
        ( .clk_i(clk)
        , .rst_i(rst)
        , .output__(done)
        );
endmodule
