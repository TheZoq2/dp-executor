use std::ops::div_pow2;

// Assumes 32x32 = 2^5 x 2^5 state space
// Performs interpolation and adds model cost tot he result
// NOTE: inputs and outputs are not registered, could impact critical path
#[no_mangle]
pipeline(11) interpolation_v1(
    clk: clock,
    x: int<32>,
    y: int<32>,
    q00: Option<int<32>>,
    q01: Option<int<32>>,
    q10: Option<int<32>>,
    q11: Option<int<32>>,
    x_step: int<29>,
    y_step: int<29>,
    // 1 / (x_step * y_step)
    step_reciprocal: int<32>,
    model_cost: int<12>
) -> Option<int<32>> {
    reg;
        let q00_ = match q00 {Some(val) => val, None => 0};
        let q01_ = match q01 {Some(val) => val, None => 0};
        let q10_ = match q10 {Some(val) => val, None => 0};
        let q11_ = match q11 {Some(val) => val, None => 0};
        // dx, dy, dxp, dyp are 27 bits representing 0..1
        // 29 bits as we want sign bit and to be able to represent 1
        let dx_: int<27> = trunc(x);
        let dy_: int<27> = trunc(y);
        let dx: int<29> = zext(dx_);
        let dy: int<29> = zext(dy_);

    reg;
        // We know this result is small and will not overflow. Safe trunc()
        let dxp: int<29> = trunc(x_step - zext(dx));
        let dyp: int<29> = trunc(y_step - zext(dy));
    reg;
        // 20 frac_bits * 27 frac bits = 47 frac bits
        let m00 = (q00_ * dxp);
        let m01 = (q10_ * dx);
        let m10 = (q01_ * dxp);
        let m11 = (q11_ * dx);
    reg;
    reg;
    reg;
        // Addition leaves fractional bits unchanged
        let sum_0 = m00 + m01;
        let sum_1 = m10 + m11;
    reg;
        // 47+27 = 74 fractional bits
        let m0 = sum_0 * dyp;
        let m1 = sum_1 * dy;
    reg;
    reg;
    reg;
        let sum3 = m0 + m1;
    reg;
        let sum_div_step = sum3 `div_pow2` zext(step_reciprocal);
        let sum3_scaled: int<32> = trunc(sum_div_step `div_pow2` 54);
        let model_cost_scaled = (sext(model_cost) << 20);
        // Scale everything back down to 12+20 fractional bits, then add cost
        match (q00, q01, q10, q11) {
            (Some(_), Some(_), Some(_), Some(_)) => {
                // We know that we have only positive costs, throw away the result
                // if it was negative (overflow)
                let result = trunc(sum3_scaled + model_cost_scaled);
                if result < 0 {
                    None()
                }
                else {
                    Some(result)
                }
            },
            _ => None()
        }
}

#[no_mangle]
pipeline(11) interpolation_v2(
    clk: clock,
    x: int<32>,
    y: int<32>,
    q00: Option<int<32>>,
    q01: Option<int<32>>,
    q10: Option<int<32>>,
    q11: Option<int<32>>,
    x_step: int<29>,
    y_step: int<29>,
    // 1 / (x_step * y_step)
    x_step_reciprocal: int<32>,
    y_step_reciprocal: int<32>,
    model_cost: int<12>
) -> Option<int<32>> {
    reg;
        let q00_ = match q00 {Some(val) => val, None => 0};
        let q01_ = match q01 {Some(val) => val, None => 0};
        let q10_ = match q10 {Some(val) => val, None => 0};
        let q11_ = match q11 {Some(val) => val, None => 0};
        // dx, dy, dxp, dyp are 27 bits representing 0..1
        // 29 bits as we want sign bit and to be able to represent 1
        let dx_: int<27> = trunc(x);
        let dy_: int<27> = trunc(y);
        let dx: int<29> = zext(dx_);
        let dy: int<29> = zext(dy_);
    reg;
        // 27*27 fractional bits => 54 fractional bits
        let dxdy = dx*dy;
        // 20 fractional bits preserved
        let term1_sum = ((q00_ - q01_) + (-q10_ + q11_));
        let term2_sum = (q10_ - q00_ );
        let term3_sum = (q01_ - q00_ );
    reg;
    reg;
    reg;
    reg;
        // 54 * 20 fractional bits = 74 frac bits
        let xy_mult = dxdy * term1_sum;
        // 27 * 20 fractional bits = 47
        let x_mult = dx*term2_sum;
        let y_mult = dy*term3_sum;
    reg;
    reg;
    reg;
        let term1 = (xy_mult `div_pow2` zext(x_step_reciprocal)) `div_pow2` zext(y_step_reciprocal);
        // 20*27 fractional bits = 47
        let term2 = x_mult `div_pow2` zext(y_step_reciprocal);
        let term3 = y_mult `div_pow2` zext(x_step_reciprocal);
    reg;
        // Scale everything back to 20 fractional bits
        let result = sext(q00_) + trunc(term1 `div_pow2` 54) + trunc(term2 `div_pow2` 27) + (term3 `div_pow2` 27);
        let model_cost_scaled = (sext(model_cost) << 20);
    reg; // NOTE: 2 stages added to make comparison with v1 easier
        // Scale everything back down to 12+20 fractional bits, then add cost
        match (q00, q01, q10, q11) {
            (Some(_), Some(_), Some(_), Some(_)) => {
                // We know that we have only positive costs, throw away the result
                // if it was negative (overflow)
                let final_result = trunc(result + model_cost_scaled);
                if final_result < 0 {
                    None()
                }
                else {
                    Some(final_result)
                }
            },
            _ => None()
        }
}









