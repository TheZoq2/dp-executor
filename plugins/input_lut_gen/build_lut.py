import argparse
import math
from os import stat
import re
from pathlib import Path

template = Path(__file__).parent.resolve() / "template.spade"

def build_inputs(source_dir: Path, word_length: int, fractional_bits: int):
    if not source_dir.exists():
        raise Exception(f"{source_dir} does not exist")
    if not source_dir.is_dir():
        raise Exception(f"{source_dir} was not a directory")

    with open(template) as f:
        template_content = f.read()

    for lut in source_dir.iterdir():
        if not lut.stem.endswith(".spade"):
            output_file = source_dir / f"{lut.stem}.spade"

            needs_write = not output_file.exists() or output_file.stat().st_mtime < lut.stat().st_mtime or output_file.stat().st_mtime < template.stat().st_mtime

            if not needs_write:
                continue

            with open(lut) as f:
                lines = f.readlines()
                lines_as_float = [float(line) for line in lines]
                lines_as_fp = [round(f * 2**fractional_bits) for f in lines_as_float]
                assert all(map(lambda f: f == 0 or math.log2(abs(f)) < word_length-1, lines_as_fp))

                content = template_content \
                    .replace("{{NAME}}", lut.stem) \
                    .replace("{{IDX_WIDTH}}", f"{math.ceil(math.log2(len(lines_as_fp)))}") \
                    .replace("{{WORD_LENGTH}}", f"{word_length}") \
                    .replace("{{VALUES}}", ",".join(map(lambda x: f"{x}", lines_as_fp)))

                with open(output_file, 'w') as out:
                    out.write(content)


# Builds a spade stub from the HEV verilog
def build_hev_stub(hev_verilog_file: Path, hev_output_file: Path):
    if not hev_verilog_file.exists():
        raise Exception(f"{hev_verilog_file} does not exist")

    rebuild = not hev_output_file.exists() or hev_output_file.stat().st_mtime < hev_verilog_file.stat().st_mtime or hev_output_file.stat().st_mtime < stat(__file__).st_mtime;

    if rebuild:
        with open(hev_verilog_file) as f:
            verilog_lines = f.readlines()

            config_lines = list(filter(lambda l: "CORE_GENERATION_INFO" in l ,verilog_lines))
            assert len(config_lines) == 1
            config_line = config_lines[0]

            matches = list(re.finditer(r"HLS_SYN_LAT=(\d+)", config_line))
            if matches is None:
                raise Exception(f"Did not find HLS_SYN_LAT in {config_line}") 
            latency = int(matches[0].group(1))

            out_definition = "struct HevOut {stage_cost: int<32>, x1next: int<32>, x2next: int<32>, fuel: int<32>, time: int<32>, teng: int<32>, fbrk: int<32>}"

            stub = f"{out_definition}\n\n #[no_mangle] pipeline({latency}) reference_pt_model(ap_clk: clock, ap_rst: bool, x1_idx: int<6>, x2_idx: int<6>, u1_idx: int<6>, u2_idx: int<6>, u3_idx: int<6>, dt: int<11>) -> HevOut __builtin__"

            with open(hev_output_file, "w") as of:
                of.write(stub)




if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("source_dir", type=Path)
    parser.add_argument("word_length", type=int)
    parser.add_argument("fractional_bits", type=int)
    parser.add_argument("hev_verilog", type=Path)
    parser.add_argument("hev_output_file", type=Path)
    args = parser.parse_args()
    build_inputs(args.source_dir, args.word_length, args.fractional_bits)
    build_hev_stub(args.hev_verilog, args.hev_output_file)

