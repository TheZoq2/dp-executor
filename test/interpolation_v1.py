# top=interpolation::interpolation_v1

from spade import *

from cocotb import triggers
from cocotb.clock import Clock

COST_SCALE = 20

async def test_case(s, clk, coords, q, x_step, y_step, step_reciprocal, expected):
    (x, y) = coords
    (q00, q01, q10, q11) = q
    s.i.x = f"{x}"
    s.i.y = f"{y}"
    s.i.q00 = f"Some({q00 << COST_SCALE}u)"
    s.i.q01 = f"Some({q01 << COST_SCALE}u)"
    s.i.q10 = f"Some({q10 << COST_SCALE}u)"
    s.i.q11 = f"Some({q11 << COST_SCALE}u)"
    s.i.x_step = f"{x_step}u"
    s.i.y_step = f"{y_step}u"
    s.i.step_reciprocal = f"{step_reciprocal}"
    # s.i.x_step_reciprocal = f"{x_step_rec}"
    # s.i.y_step_reciprocal = f"{y_step_rec}"
    await triggers.ClockCycles(clk, 10, rising = False)
    s.o.assert_eq(f"Some({expected}u)")


@cocotb.test()
async def corner_points(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i

    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    await FallingEdge(clk)

    step1 = 1 << 27

    s.i.model_cost = "0"
    # On top of q00
    await test_case(s, clk, (0, 0), (5, 6, 7, 8), step1, step1, 0, 5 << COST_SCALE)
    # On top of q01
    await test_case(s, clk, ((1 << 27) - 1, 0), (5, 6, 7, 8), step1, step1, 0, (7 << COST_SCALE)-1)
    # On top of q10
    await test_case(s, clk, (0, (1 << 27) - 1), (5, 6, 7, 8), step1, step1, 0, (6 << COST_SCALE)-1)
    # On top of q11
    await test_case(s, clk, ((1 << 27) - 1, (1 << 27) - 1), (5, 6, 7, 8), step1, step1, 0, (8 << COST_SCALE)-1)


@cocotb.test()
async def planes(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i

    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    await FallingEdge(clk)

    step1 = 1 << 27

    s.i.model_cost = "0"
    # Flat plane
    await test_case(s, clk, ((1 << 27) - 1, (1 << 27) - 1), (9, 9, 9, 9), step1*2, step1*2, 2, (9 << COST_SCALE))

    # Sloped plane
    await test_case(s, clk, ((1 << 27) - 1, (1 << 27) - 1), (9, 9, 11, 11), step1*2, step1*2, 2, (10 << COST_SCALE) - 1)

    # Sloped plane in the other direction
    await test_case(s, clk, ((1 << 27) - 1, (1 << 27) - 1), (9, 11, 9, 11), step1*2, step1*2, 2, (10 << COST_SCALE) - 1)
