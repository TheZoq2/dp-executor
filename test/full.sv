`include "test/vatch/main.v"

module full_tb();
    `SETUP_TEST
    // 30 cycles to let resets propagate into pipelines
    `CLK_AND_RST(clk, rst, 30)

    wire[31:0] result;

    integer test_case;

    reg start;
    reg[9:0] read_t;
    reg[5:0] read_x;
    reg[5:0] read_y;

    wire[31:0] out_cost;
    wire done;

    e_main uut
        ( ._i_clk(clk)
        , ._i_rst(rst)
        , ._i_start(start)
        , ._i_read_pos({read_t, read_x, read_y})
        , .__output({done, out_cost})
        );


    integer t;
    integer x;
    integer y;

    real out_cost_r;

    initial begin
        test_case <= 0;

        @(negedge rst)
        start <= 1;
        @(negedge clk)
        start <= 0;
        // repeat(3 * 3 * 6 * 10 * 10 * 10) @(negedge clk);

        // Wait for computation to finish
        @(posedge done)
        // repeat(3 * 3 * 6 * 10 * 10 * 10) @(negedge clk);
        // Wait for pipeline to flush
        repeat(20) @(negedge clk);


        for (t = 0; t < 3; t = t+1) begin
            $display("t: %0d", t);
            for (x = 0; x < 10; x = x+1) begin
                for (y = 0; y < 10; y = y+1) begin
                    read_t = t;
                    read_x = x;
                    read_y = y;

                    repeat(33) @(negedge clk);
                    if ($isunknown(out_cost)) begin
                        $write("X ");
                    end
                    else begin
                        out_cost_r = $itor(out_cost) / (1 << 20);
                        $write("%g ", out_cost_r);
                    end

                    // $display("(%h, %h, %h): %h", t, x, y, out_cost);
                end
                $display("");
            end
        end


        #10
        `END_TEST
    end
endmodule
