`include "test/vatch/main.v"

module interpolation_tb();
    `SETUP_TEST
    `CLK_AND_RST(clk, rst, 1)

    reg[31:0] x;
    reg[31:0] y;
    reg[31:0] q00;
    reg[31:0] q01;
    reg[31:0] q10;
    reg[31:0] q11;
    reg[28:0] x_step;
    reg[29:0] y_step;
    reg[31:0] x_step_reciprocal;
    reg[31:0] y_step_reciprocal;
    wire[31:0] result;
    wire option_kind;

    integer test_case;

    e_interpolation_v2 uut
        ( ._i_clk(clk)
        , ._i_x(x)
        , ._i_y(y)
        , ._i_q00(q00)
        , ._i_q01(q01)
        , ._i_q10(q10)
        , ._i_q11(q11)
        , ._i_x_step(x_step)
        , ._i_y_step(y_step)
        , ._i_x_step_reciprocal(x_step_reciprocal)
        , ._i_y_step_reciprocal(y_step_reciprocal)
        , ._i_model_cost(0)
        , .__output({option_kind, result})
        );

    `define TEST_CASE(X, Y, Q00, Q01, Q10, Q11, X_STEP, Y_STEP, X_STEP_REC, Y_STEP_REC) \
        x <= X; \
        y <= Y; \
        q00 <= Q00 << COST_SCALE; \
        q01 <= Q01 << COST_SCALE; \
        q10 <= Q10 << COST_SCALE; \
        q11 <= Q11 << COST_SCALE; \
        x_step <= X_STEP; \
        y_step <= Y_STEP; \
        x_step_reciprocal <= X_STEP_REC; \
        y_step_reciprocal <= Y_STEP_REC; \
        test_case <= test_case + 1;


    localparam step1 = 1 << 27;
    localparam COST_SCALE = 20;
    initial begin
        test_case <= 0;
        @(negedge clk);

        // On top of Q00
        `TEST_CASE(0, 0, 5, 6, 7, 8, step1, step1, 0, 0);
        repeat (10) @(negedge clk);
        `ASSERT_EQ(result, 5 << COST_SCALE)

        // On top of Q01
        `TEST_CASE((1 << 27) - 1, 0, 5, 6, 7, 8, step1, step1, 0, 0);
        repeat (10) @(negedge clk);
        `ASSERT_EQ(result + 1, 6 << COST_SCALE)

        // On top of Q10
        `TEST_CASE(0, (1 << 27)-1, 5, 6, 7, 8, step1, step1, 0, 0);
        repeat (10) @(negedge clk);
        `ASSERT_EQ(result + 1, 7 << COST_SCALE)

        // On top of Q11
        `TEST_CASE((1 << 27)-1, (1 << 27)-1, 5, 6, 7, 8, step1, step1, 0, 0);
        repeat (10) @(negedge clk);
        `ASSERT_EQ(result + 1, 8 << COST_SCALE)

        // NOTE: +1 here is because of some roundoff error that is *probably*
        // fine

        // A flat plane will map any (x, y) to the value
        `TEST_CASE((1 << 27)-1, (1 << 27)-1, 9, 9, 9, 9, step1*2, step1*2, 1, 1);
        repeat (10) @(negedge clk);
        `ASSERT_EQ(result, 9 << COST_SCALE)

        // A sloped plane will map the center to the middle value
        `TEST_CASE((1 << 27)-1, (1 << 27)-1, 9, 9, 11, 11, step1*2, step1*2, 1, 1);
        repeat (10) @(negedge clk);
        `ASSERT_EQ(result+1, 10 << COST_SCALE)

        // And in the other direction
        `TEST_CASE((1 << 27)-1, (1 << 27)-1, 9, 11, 9, 11, step1*2, step1*2, 1, 1);
        repeat (10) @(negedge clk);
        `ASSERT_EQ(result+1, 10 << COST_SCALE)

        #10
        `END_TEST
    end

endmodule
