# top=memory::memory_test

from spade import *

from cocotb import triggers
from cocotb.clock import Clock

async def write(s, clk, x, y, write_val):
    s.i.x = x
    s.i.y = y
    s.i.write = f"Some({write_val})"
    await FallingEdge(clk)
    s.i.write = "None()"

async def read(s, clk, x, y):
    s.i.x = x
    s.i.y = y
    s.i.write = "None()"
    await FallingEdge(clk)

@cocotb.test()
async def memory_tb(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i

    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    await read(s, clk, "0", "0")


    # Set up the first square as
    # 5   6
    # 7   8
    await write(s, clk, "0", "0", "5")
    await write(s, clk, "1", "0", "6")
    await write(s, clk, "0", "1", "7")
    await write(s, clk, "1", "1", "8")


    # Read read the values in the newly added square
    await read(s, clk, "0", "0")
    await triggers.ClockCycles(clk, 4, rising = False)
    s.o.q00.assert_eq("Some(5)")
    s.o.q10.assert_eq("Some(6)")
    s.o.q01.assert_eq("Some(7)")
    s.o.q11.assert_eq("Some(8)")


    # Set up more values to get
    # 5   6  9
    # 7   8 10
    # 11 12 13
    await write(s, clk, "2", "0", "9")
    await write(s, clk, "2", "1", "10")
    await write(s, clk, "2", "2", "13")
    await write(s, clk, "0", "2", "11")
    await write(s, clk, "1", "2", "12")

    # Read read the values starting at (1, 0)
    await read(s, clk, "1", "0")
    await triggers.ClockCycles(clk, 4, rising = False)

    s.o.q00.assert_eq("Some(6)")
    s.o.q10.assert_eq("Some(9)")
    s.o.q01.assert_eq("Some(8)")
    s.o.q11.assert_eq("Some(10)")


    # Read read the values starting at (0, 1)
    await read(s, clk, "0", "1")
    await triggers.ClockCycles(clk, 4, rising = False)
    s.o.q00.assert_eq("Some(7)")
    s.o.q10.assert_eq("Some(8)")
    s.o.q01.assert_eq("Some(11)")
    s.o.q11.assert_eq("Some(12)")

    # Read read the values starting at (1, 1)
    await read(s, clk, "1", "1")
    await triggers.ClockCycles(clk, 4, rising = False)
    s.o.q00.assert_eq("Some(8)")
    s.o.q10.assert_eq("Some(10)")
    s.o.q01.assert_eq("Some(12)")
    s.o.q11.assert_eq("Some(13)")
