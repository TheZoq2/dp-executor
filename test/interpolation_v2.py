# top=interpolation::interpolation_v2

from spade import *

from cocotb import triggers
from cocotb.clock import Clock

COST_SCALE = 20

async def test_case(s, clk, coords, q, x_step, y_step, reciprocal, expected):
    (x, y) = coords
    (q00, q01, q10, q11) = q
    (x_step_rec, y_step_rec) = reciprocal
    s.i.x = f"{x}"
    s.i.y = f"{y}"
    s.i.q00 = f"Some({q00 << COST_SCALE}u)"
    s.i.q01 = f"Some({q01 << COST_SCALE}u)"
    s.i.q10 = f"Some({q10 << COST_SCALE}u)"
    s.i.q11 = f"Some({q11 << COST_SCALE}u)"
    s.i.x_step = f"{x_step}u"
    s.i.y_step = f"{y_step}u"
    s.i.x_step_reciprocal = f"{x_step_rec}"
    s.i.y_step_reciprocal = f"{y_step_rec}"
    await triggers.ClockCycles(clk, 11, rising = False)
    s.o.assert_eq(f"Some({expected})")

async def setup_test(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i

    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    await FallingEdge(clk)
    return (s, clk)


@cocotb.test()
async def corner_q00(dut):
    (s, clk) = await setup_test(dut)

    step1 = 1 << 27

    s.i.model_cost = "0"
    await test_case(s, clk, (0, 0), (5, 6, 7, 8), step1, step1, (0, 0), 5 << COST_SCALE)


@cocotb.test()
async def corner_q01(dut):
    (s, clk) = await setup_test(dut)

    step1 = 1 << 27

    s.i.model_cost = "0"
    await test_case(s, clk, (step1 - 1, 0), (5, 6, 7, 8), step1, step1, (0, 0), (7 << COST_SCALE))


@cocotb.test()
async def corner_q10(dut):
    (s, clk) = await setup_test(dut)

    step1 = 1 << 27

    s.i.model_cost = "0"
    await test_case(s, clk, (0, step1 - 1), (5, 6, 7, 8), step1, step1, (0, 0), (6 << COST_SCALE))


@cocotb.test()
async def corner_q11(dut):
    (s, clk) = await setup_test(dut)

    step1 = 1 << 27

    s.i.model_cost = "0"
    # On top of q11
    await test_case(s, clk, (step1 - 1, step1 - 1), (5, 6, 7, 8), step1, step1, (0, 0), (8 << COST_SCALE))


@cocotb.test()
async def planes(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i

    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    await FallingEdge(clk)

    step1 = 1 << 27

    s.i.model_cost = "0"
    # Flat plane
    await test_case(s, clk, ((1 << 27) - 1, (1 << 27) - 1), (9, 9, 9, 9), step1*2, step1*2, (1, 1), (9 << COST_SCALE))

    # Sloped plane
    await test_case(s, clk, ((1 << 27) - 1, (1 << 27) - 1), (9, 9, 11, 11), step1*2, step1*2, (1, 1), (10 << COST_SCALE))

    # Sloped plane in the other direction
    await test_case(s, clk, ((1 << 27) - 1, (1 << 27) - 1), (9, 11, 9, 11), step1*2, step1*2, (1, 1), (10 << COST_SCALE))
