// top=main::main_verilator

#include <cstdint>
#include <ranges>
#include <format>
#include <iostream>
#define TOP main_verilator
#include <verilator_util.hpp>

void print_table_cell(float value) {
    int r,g,b,fg;
    if (value < 64) {
        r = int(value*4);
        g = (r & 0x0f) << 4;
        b = (r & 0b00111100) << 2;
        fg = r < 128 ? 255 : 0;
    } else {
        r = 255;
        g = 255;
        b = 255;
        fg = 255;
    }
    std::cout << std::format("\x1b[38;2;{};{};{};249m\x1b[48;2;{};{};{};249m{:>6}\x1b[0m", fg,fg,fg,r, g, b, value);
}

#define TICK \
    dut->clk = 1; \
    ctx->timeInc(1); \
    dut->eval(); \
    dut->clk = 0; \
    ctx->timeInc(1); \
    dut->eval(); \

TEST_CASE(sweep, {

    s.i->rst = "true";
    s.i->start = "false";
    TICK
    s.i->rst = "false";
    s.i->start = "true";
    TICK
    s.i->start = "false";
    TICK

    int num_cycles = 0;
    while (!dut->done) {
        TICK

        if (num_cycles % 1000 == 0) {
            std::cout << num_cycles << std::endl;
        }

        if (dut->done == 1) {
            break;
        }

        num_cycles += 1;
    }

    for(int t = 0; t < 3; t++) {
        std::cout << std::format("t: {}", t) << std::endl;

        std::cout << "x\\y";

        for (int i = 0; i < 10; i++) {
            std::cout << std::format("{:>6}", i);
        }
        std::cout << std::endl;
        std::cout << t << std::endl;

        for(auto x : std::views::iota(0, 10)) {
            std::cout << std::format("{:3}", x);
            for(auto y : std::views::iota(0, 10)) {
                s.i->read_pos = std::format("({}, {}, {})", t, x, y);
                for(auto _c : std::views::iota(0, 33)) {
                    TICK
                }
                int32_t val = dut->mem_out;

                float val_float = (float) val / (float) (1 << 20);
                print_table_cell(val_float);
            }
            std::cout << std::endl;
        }
    }

    return 1;
})

MAIN
