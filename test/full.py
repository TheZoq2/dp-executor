# top=main::main

import sys
import os
sys.path.append(os.environ["SWIM_ROOT"])
from py_util import print_table_cell

from spade import *

from cocotb import triggers
from cocotb.clock import Clock

@cocotb.test()
async def full_run(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i

    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    s.i.rst = "true"
    s.i.start = "false"
    await triggers.ClockCycles(clk, 33, rising = False)
    s.i.rst = "false"
    s.i.start = "true"
    await FallingEdge(clk)
    s.i.start = "false"


    done = s.o.done
    num_cycles = 0;
    while True:
        await triggers.First(Edge(dut.output__), triggers.ClockCycles(clk, 10_000))

        print(num_cycles)

        if done.is_eq("true"):
            print(f"Done after {num_cycles} cycles")
            break
        num_cycles += 1

    for t in range(0, 3):
        print(f"t: {t}")

        print("x\\y",end='')
        for i in range(0,10):
            print(f"{i:>6}", end="")
        print()

        print(f"{t}")
        for x in range(0, 10):
            print(f"{i:>3}", end='')
            for y in range(0, 10):
                s.i.read_pos = f"({t}, {x}, {y})"
                await triggers.ClockCycles(clk, 33);
                val = s.o.mem_out.value()

                if val == "UNDEF":
                    val = None
                else:
                    val = int(val) / (1 << 20)
                print_table_cell(val)
            print("")




