import pdb

from py_util import print_table_cell

FP_SCALE=2**27;

def model(t, x, y, u1, u2, u3):
    # pdb.set_trace()
    t = 10-t-2
    x_value = (x * FP_SCALE);
    y_value = (y * FP_SCALE) * 2;
    u1_value = u1-15;
    u2_value = u2-15;
    u3_value = u3;

    x_next = x_value + (u1_value << 24)
    y_next = y_value + (u2_value << 24) + (u3_value << 23)
    # if x_next < 0 or x_next > (9*FP_SCALE) or y_next < 0 or y_next > (9 * FP_SCALE):
    if x_next < 0 or y_next < 0:
        cost = 20
        # return (cost, min(max(0, x_next), (8 * FP_SCALE)), min(max(0, y_next), (8 * FP_SCALE))) 
        return (cost, 0, 0) 

    cost = abs(t - (u1 + u2 + u3))
    return (cost, x_next, y_next)

# Cost to go is the cost to go of the time step in which to do interpolation
def interp(cost_to_go, x, y, x_step, y_step):
    x_lower = x >> 27;
    y_lower = y >> 27;
    x_upper = x_lower + 1;
    y_upper = y_lower + 1;

    dx = x & (FP_SCALE)-1
    dy = y & (FP_SCALE)-1
    dx_ = x_step-dx
    dy_ = y_step-dy

    if x_lower < 0 or x_upper >= len(cost_to_go) or y_lower < 0 or y_upper >= len(cost_to_go[0]):
        return None

    q00 = cost_to_go[x_lower][y_lower]
    q01 = cost_to_go[x_lower][y_upper]
    q10 = cost_to_go[x_upper][y_lower]
    q11 = cost_to_go[x_upper][y_upper]

    if any(map(lambda x: x is None, [q00, q01, q10, q11])):
        return None

    return (dx_*dy_*q00 +
            dx_*dy*q01 +
            dx*dy_*q10 +
            dx*dy*q11) / (x_step * y_step)


def interp_v2(cost_to_go, x, y, x_step, y_step, x_step_reciprocal, y_step_reciprocal):
    x_lower = x >> 27;
    y_lower = y >> 27;
    x_upper = x_lower + 1;
    y_upper = y_lower + 1;
    if x_lower < 0 or x_upper >= len(cost_to_go) or y_lower < 0 or y_upper >= len(cost_to_go[0]):
        return None


    dx = x & (FP_SCALE)-1
    dy = y & (FP_SCALE)-1
    dxdy = dx*dy

    q00 = cost_to_go[x_lower][y_lower]
    q01 = cost_to_go[x_lower][y_upper]
    q10 = cost_to_go[x_upper][y_lower]
    q11 = cost_to_go[x_upper][y_upper]

    if any(map(lambda x: x is None, [q00, q01, q10, q11])):
        return None

    term1_sum = ((q00-q01) + (-q10+q11))
    term2_sum = q10-q00
    term3_sum = q01-q00

    xy_mult = dxdy * term1_sum
    x_mult = dx*term2_sum
    y_mult = dy*term3_sum

    term1 = (xy_mult / 2**(x_step_reciprocal+y_step_reciprocal))
    term2 = x_mult / 2**y_step_reciprocal
    term3 = y_mult / 2**x_step_reciprocal

    result = q00 + (term1 / 2**54) + (term2 / 2**27) + (term3 / 2**27)
    return result


def eval():
    X_STEPS = 9;
    Y_STEPS = 9;
    CTG_SIZE_X = X_STEPS + 1;
    CTG_SIZE_Y = Y_STEPS + 1;
    U1_STEPS = 7;
    U2_STEPS = 7;
    U3_STEPS = 6;
    cost_to_go = [[ [0 for _ in range(0, CTG_SIZE_X)]
               for _ in range(0, CTG_SIZE_Y)]
               for _ in range(0, 4)]

    X_MAX = X_STEPS * FP_SCALE
    Y_MAX = Y_STEPS * FP_SCALE

    # for t in [10-i-2 for i in range(0, 3)]:
    for t in range(0, 3):
        print(f"Evaluating {t}")
        for x in range(0, X_STEPS+1):
            for y in range(0, Y_STEPS+1):
                best = None
                for u1 in range(0, U1_STEPS+1):
                    for u2 in range(0, U2_STEPS+1):
                        for u3 in range(0, U3_STEPS+1):
                            if t == 1 and x == 1 and y == 2 and u1 == 7 and u2 == 6 and u3 == 1:
                                #pdb.set_trace()
                                pass

                            (c_local, x_next, y_next) = model(t, x, y, u1, u2, u3)

                            ctg_map = cost_to_go[t-1] if t != 0 else [ [0 for _ in range(0, CTG_SIZE_X)] for _ in range(0, CTG_SIZE_Y)]
                            # c_prev = interp(ctg_map, x_next, y_next, FP_SCALE, FP_SCALE)
                            c_prev = interp_v2(ctg_map, x_next, y_next, FP_SCALE, FP_SCALE, 0, 0)

                            if c_prev is None:
                                continue


                            c = c_local + c_prev

                            if best is None or c < best:
                                best = c

                            # if x == 3 and y == 1 and t == 2 and not best is None:
                            #     print(f"Has value {best} at u1: {u1}, u2: {u2}, u3: {u3}")

                cost_to_go[t][x][y] = best

    for (t, time_step) in enumerate(cost_to_go):
        print(f"t: {t}")

        print("x\\y",end='')
        for (i, _) in enumerate(time_step[0]):
            print(f"{i:>6}", end="")
        print()

        for (i, x) in enumerate(time_step):
            print(f"{i:>3}", end='')
            for y in x:
                print_table_cell(y)
            print("")



def test(expr, expected):
    if expr != expected:
        print(f"{expr} != {expected}")
        assert expr == expected

if __name__ == "__main__":
    # model(0, 2, 9, 7, 7, 6)
    # Test interp code
    def scale_map(m):
        return list(map(lambda inner: list(map(lambda x: x * 2**27, inner)), m))
    # Landing on x or y values produces those values
    assert interp(scale_map([[0, 1], [2, 3]]), 0, 0, 2**27, 2**27) == 0
    assert interp(scale_map([[0, 1], [2, 3]]), 1, 0, 2**27, 2**27) == 2
    assert interp(scale_map([[0, 1], [2, 3]]), 0, 1, 2**27, 2**27) == 1
    assert interp(scale_map([[0, 1], [2, 3]]), 1, 1, 2**27, 2**27) == 3

    test(interp([[0, 0], [1, 1]], 1, 0, 2, 1), 0.5)
    test(interp([[0, 1], [0, 1]], 0, 1, 1, 2), 0.5)
    eval()
